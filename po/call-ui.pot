# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the call-ui package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: call-ui\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-07-01 18:38+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: examples/cui-demo-window.ui:8
msgid "Preferences"
msgstr ""

#: examples/cui-demo-window.ui:15 examples/cui-demo-window.ui:38
msgid "Call-UI Demo"
msgstr ""

#: examples/cui-demo-window.ui:165
msgid "Back"
msgstr ""

#: examples/cui-demo-window.ui:217
msgid "Welcome to Call-UI Demo"
msgstr ""

#: examples/cui-demo-window.ui:218
msgid "This is a tour of the features the library has to offer."
msgstr ""

#: examples/cui-demo-window.ui:222
msgid "Welcome"
msgstr ""

#: examples/cui-demo-window.ui:237
msgid "Call Display"
msgstr ""

#: src/cui-call-display.c:291
msgid "Calling…"
msgstr ""

#: src/cui-call-display.c:340 src/cui-call-display.c:569
msgid "Unknown"
msgstr ""

#: src/cui-call-display.ui:37
msgid "Incoming phone call"
msgstr ""

#: src/cui-call-display.ui:137
msgid "Mute"
msgstr ""

#: src/cui-call-display.ui:174
msgid "Speaker"
msgstr ""

#: src/cui-call-display.ui:210
msgid "Add call"
msgstr ""

#: src/cui-call-display.ui:255
msgid "Hold"
msgstr ""

#: src/cui-call-display.ui:291
msgid "Dial Pad"
msgstr ""

#: src/cui-call-display.ui:347
msgid "Hang up"
msgstr ""

#: src/cui-call-display.ui:378
msgid "Answer"
msgstr ""

#: src/cui-call-display.ui:463
msgid "Hide the dial pad"
msgstr ""

#: src/cui-encryption-indicator.ui:23
msgid "This call is not encrypted"
msgstr ""

#: src/cui-encryption-indicator.ui:45
msgid "This call is encrypted"
msgstr ""
